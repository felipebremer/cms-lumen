<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert(
            [
                'name' => 'SuperAdmin',
                'email' => 'super@admin.com',
                'role' => 'SUPERADMIN',
                'password' => Hash::make('randomPassword')
            ]
        );
    }
}