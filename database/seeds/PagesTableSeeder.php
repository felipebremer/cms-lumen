<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('pages')->truncate();
        DB::table('pages')->insert([
            [
                'Title' => 'home',
                'slug' => '/',
                'status' => true,
                'default' => true,
                'content' => '
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>MoreOne CMS</title>
                    </head>
                    <body>
                        HOME CONTENT
                    </body>
                    </html>
                ',
                'content_preview' => '
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>MoreOne CMS</title>
                    </head>
                    <body>
                        HOME CONTENT PREVIEW
                    </body>
                    </html>
                ',
            ],
            [
                'Title' => '404',
                'slug' => '/404',
                'status' => true,
                'default' => true,
                'content' => '
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>MoreOne CMS - 404</title>
                    </head>
                    <body>
                        404
                    </body>
                    </html>
                ',
                'content_preview' => '
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>MoreOne CMS</title>
                    </head>
                    <body>
                        404 PREVIEW
                    </body>
                    </html>
                ',
            ],
        ]);
    }
}