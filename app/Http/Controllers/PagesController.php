<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use Auth;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all the pages
        $pages = Page::all();
        return response()->json(['status' => 'success', 'result' => $pages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'status' => 'required',
            'default' => 'required',
            'content_preview' => 'required',
        ]);

        $page = Page::create($request->all());

        if ($page) {
            return response()->json($page, 201);
        }
        return response()->json(['status' => 'failed']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        if ($page) {
            return response()->json($page, 201);
        }
        return response()->json(['status' => 'failed']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'filled',
            'slug' => 'filled',
            'status' => 'filled',
            'default' => 'filled',
            'content_preview' => 'filled',
        ]);
        $page = Page::findOrFail($id);
        if ($page) {
            $page->update($request->all());
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $page = Page::findOrFail($id);
        if ($page) {
            $page->delete();
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'failed']);
    }
}