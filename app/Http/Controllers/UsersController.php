<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['authenticate']]);

    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->input('username'))->first();

        if (!isset($user)) {
            return response()->json([
                'status' => 'error',
                'message' => 'user or pass invalid'
            ], 401);
        } else if (Hash::check($request->input('password'), $user->password)) {
            $api_token = base64_encode(str_random(40));
            $user->update(['api_token' => "$api_token"]);
            return response()->json([
                'status' => 'success',
                'token' => $api_token
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'user or pass invalid'
            ], 401);
        }
    }

    public function createNewUser(Request $request)
    {
        $response = $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required'
            ]
        );

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();

        if ($user->save()) {
            $response = response()->json(
                [
                    'response' => [
                        'created' => true,
                        'userId' => $user->id
                    ]
                ],
                201
            );
        }
        return $response;
    }
}