<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

    protected $fillable = [
        'name',
        'content',
        'content_preview',
        'utm_source',
        'utm_medium',
        'utm_campaign'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public static $rules = [
        'title' => 'required',
        'slug' => 'required',
        'status' => 'required',
        'default' => 'unique|unique_with:slug',
        'content_preview' => 'required',
    ];

    // Relationships

}
