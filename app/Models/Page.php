<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $fillable = [
        'title',
        'slug',
        'status',
        'default',
        'content',
        'content_preview',
        'date_init',
        'date_end'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date_init',
        'date_end'
    ];

    public static $rules = [
        'title' => 'required',
        'slug' => 'required',
        'status' => 'required',
        'default' => 'unique|unique_with:slug',
        'content_preview' => 'required',
    ];
}
